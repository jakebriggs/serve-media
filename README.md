# serve-media #

Play around app for serving media - using nodejs/expressjs/binaryjs on the backend and angularjs on the front

Caches the music and covers using chromes FileAPI persistantly and localy - so it might work on firefox, but only chrome really is supported.

* At the moment, I can't play the audio while streaming it, but I can once its cached localy.
* Also, I need to test that the chunks are being saved in the correct order.
* Also, I need to tidy up the code a bit....
* Also, if there is no cover the previous cover doesn't go away
* Also, I need to make proper css
* Also, charcters like & and ' break the server....
* Also, I probably need to look at using removeObjectURL for each createObjectURL I use, to avoid memory leaks
* Also, I rename the file to remove the path - like Artist/Album/song.mp3 gets renamed to Artist-Album-song.mp3. I did this for quick prototyping but this could end up having filename collisions.