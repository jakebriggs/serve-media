var http = require('http');
var express = require('express');
var app = express();
var BinaryServer = require('binaryjs').BinaryServer;
var fs = require('fs');
var Bunyan = require('bunyan');
var coverclient = null;
var musicclient = null;

var mediaroot = '/data/music/';


var log = new Bunyan({
    name: 'music',
    streams: [{
        stream: process.stdout,
        level: 'debug'
    }, {
        path: 'server.log',
        level: 'debug'
    }],
    serializers: {
        req: Bunyan.stdSerializers.req
    }
});

app.use(express.static('public'));

app.get('/getfile', function (req, res) {
    log.debug('getfile ');
    var path = decodeURI(req.query.path);
    
    log.debug(' - ' + path);
   
    var send = musicclient.createStream(path);
    var file = fs.createReadStream(mediaroot + req.query.path);

    file.pipe(send);
    
    res.send('server app.get getfile: sending file ' + path);    
});

app.get('/getcover', function (req, res) {
    log.debug('getcover ');
    var path = decodeURIComponent(req.query.path);
    log.debug(' - ' + path);
    
    var send = coverclient.createStream(path);
    var file = fs.createReadStream(mediaroot + req.query.path);

    file.pipe(send);

    res.send('server app.get getcover: sending cover ' + path);  
});

app.get('/getfiles', function (req, res) {
    log.debug('getfiles ');
    log.debug(' - ' + req.query.path);
    var path = decodeURI(req.query.path);

    if(!req.query.path){
	    path = '';
    } 
    log.debug(' - path is: ' + path);

    var thedatas = [];
    var thefiles;
    
    thefiles = fs.readdirSync(mediaroot + path);
    log.debug(thefiles.length);

    for(var i=0; i<thefiles.length; i++){ 
      var stat = fs.statSync(mediaroot + path + '/' + thefiles[i]);
      var add = false;
      var type = '';

      //log.debug(stat);
      
      if(stat.isFile()){

        if(thefiles[i].endsWith('.mp3')){
          add = true;
          type = 'music';
        } else if(thefiles[i].endsWith('.jpg')){
          add = true;
          type = 'cover';
        }

      } else if (stat.isDirectory()){
	    add = true;
        type = 'dir';
      }
	
	  if(add) {
        thedatas.push({"filename": path + '/' + thefiles[i],"id": i, "type": type});
      }
    }  
	
    res.json(thedatas);
});

var server = http.createServer(app).listen(3000, '0.0.0.0', function () {log.debug('Example app listening on port 3000!');});

// Start Binary.js servers
var coverserver = new BinaryServer({server: server, path: '/cover-endpoint'});

// Wait for new user connections
coverserver.on('connection', function(client){
  log.debug('Cover Client Connected');
  coverclient = client;
});

var musicserver = new BinaryServer({server: server, path: '/music-endpoint'});

// Wait for new user connections
musicserver.on('connection', function(client){
  log.debug('Music Client Connected');
  musicclient = client;
});

