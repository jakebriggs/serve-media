var musicPlayer = angular.module('musicPlayer',[]);

musicPlayer.filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);

musicPlayer.controller('MusicController', ['$scope', '$http', function($scope,$http) {
    $scope.currenttrack = '';
    $scope.currentcover = '';
    $scope.currentoperation = '';
    $scope.status = 'Nothing Happening';
    $scope.mincachebump = 100 * 1024 * 1024; // 100 MB
    $scope.currentdir = '/';

    var audioContext = new AudioContext();
    var audioBufferSource = null;
    var startTime = null;
    var allAudioBufferSourceNode = [];

    /* if we don't have a little bit of quota, we can't make directories later.... */
    /* so lets ask for mincachebump bytes - if we have it, we don't get asked for it again */
    navigator.webkitPersistentStorage.requestQuota(
        $scope.mincachebump,
    function(grantedBytes) {
        console.log('rq: granted ' + grantedBytes + ' total bytes');
    },
    function(e) {
        console.log('rq: Error', e);
    }
    );

    /* get the root files */
    $http.get('/getfiles')
    .success(function(data) {
        $scope.data = data;
    })
    .error(function(data) {
        console.log('Error: ' + data);
    });

    $http.get('/getcover?path=nocover.jpg')
    .success(function(data) {
        $scope.status = data;
    })
    .error(function(data) {
        $scope.status = 'Initial cover get: Error: ' + data;
    });

    var coverclient = new BinaryClient('ws://' + document.location.host + '/cover-endpoint');
    var musicclient = new BinaryClient('ws://' + document.location.host + '/music-endpoint');

    console.log('Started two binary clients to ' + document.location.host);


    queueChunkToPlay = function(buffer) {
        var audioBufferSource = audioContext.createBufferSource();
        audioBufferSource.buffer = buffer;
        audioBufferSource.connect(audioContext.destination);

        audioBufferSource.onended = function() {
            console.log('audioBufferSource has finished playing');
        }
        console.log('starttime is ' + startTime);

        audioBufferSource.start(audioContext.currentTime + startTime);
        allAudioBufferSourceNode.push(audioBufferSource);
    };

    setCover = function(blob) {
        $scope.currentcover = (window.URL || window.webkitURL).createObjectURL(blob);
        console.log('setCover: currentcover ' + $scope.currentcover);
        $scope.status = 'current cover should be ' + $scope.currentcover;
        $scope.$apply();
    };

    findOrRequestFile = function(name,requestFileCallback,findFileCallback) {
        var fn = name.split('/').join('-');

        console.log('findOrRequestFile starting :' + name);

        /* look for the file */
        window.webkitRequestFileSystem(PERSISTENT, $scope.mincachebump,
        function(fsobject) {
            console.log('findOrRequestFile window.webkitRequestFileSystem starting' + fsobject);

            fsobject.root.getDirectory(
                'music',
            {create: true, exclusive: false},

            function(x) {
                console.log('blaa: ' + x);
            },

            function(x) {
                console.log('Error: ' + x);
            }
            );

            fsobject.root.getDirectory('music', {create: true, exclusive: false}, function(dirEntry) {
                console.log('findOrRequestFile got a dir');

                var dirReader = dirEntry.createReader();
                var entries = [];

                var listEntries = function() {
                    dirReader.readEntries(function(results) {
                        if (!results.length) {
                            var found = false;

                            entries.forEach(function(entry, i) {
                                if(fn == entry.name) {
                                    found = true;
                                }
                            });

                            /* If we didn't find the file we need to make the file so it can be written to later when we get data */
                            /* since we now know the file exists since we found it or just made it, we can play it immediately */
                            /* If we just made it, it'll be empty, but hopefully it still works */

                            if(!found) {
                                console.log('Requesting file ' + name + ' now');
                                requestFileCallback(name);
                            } else {
                                console.log('Playing file ' + name + ' now');

                                /* play immediately */
                                findFileCallback(fn);
                            }
                        } else {
                            entries = entries.concat(Array.prototype.slice.call(results || [], 0));
                            listEntries();
                        }
                    }, function(e) {
                        console.log('Error: ' + e);
                    });
                };

                listEntries();
            });
        });
    };

    saveFile = function(blob, saveCallback) {

        var requestedBytes = blob.size;
        //console.log('saveFile: asking for ' + requestedBytes + ' more bytes');

        navigator.webkitPersistentStorage.queryUsageAndQuota (
        function(usedBytes, grantedBytes) {
            //console.log('saveFile: we are using ', usedBytes, ' of ', grantedBytes, 'bytes, and we want to use ' + (usedBytes + requestedBytes) + ' bytes');

            // Are we about to go over?
            if(usedBytes + requestedBytes < grantedBytes) {
                // nope, lets request 0 more bytes since we have enough, and we don't need any more yet
                // and we don't want to promt the user every time
                //console.log('saveFile: we dont need any more storage');
                requestedBytes = 0;
            } else {
                //console.log('saveFile: we do need more storage');
                // lets just grab a whole chunk - 100 MB at least
                if(requestedBytes < $scope.mincachebump) {
                    requestedBytes = $scope.mincachebump;
                }
                console.log('saveFile: we need an extra ' + requestedBytes + ' bytes');
            }

            navigator.webkitPersistentStorage.requestQuota (
                requestedBytes + grantedBytes,
            function(grantedBytes) {
                window.webkitRequestFileSystem(PERSISTENT, grantedBytes, function(fsobject) {
                    //console.log('saveFile: granted ' + grantedBytes + ' total bytes');
                    appendFile(fsobject,blob,saveCallback);
                });
            },
            function(e) {
                console.log('saveFile: Error', e);
            }
            );
        },
        function(e) {
            console.log('saveFile: Error', e);
        }
        );

    }

    writeFile = function(fs,blob,writeCallback) {
        //console.log('writeFile: blob ' + blob.name + ' is ' + blob.size + ' bytes ');

        var fn = blob.name;
        //console.log('writeFile: fn ' + fn );

        fs.root.getDirectory('music', {create: true}, function(dirEntry) {
            dirEntry.getFile(fn, {create: true, exclusive: false}, function(fileEntry) {
                // Create a FileWriter object for our FileEntry, and write out blob.
                fileEntry.createWriter(function(fileWriter) {
                    fileWriter.onerror = function(e) {
                        console.log('writeFile: Error', e);
                    };
                    fileWriter.onwriteend = function(e) {
                        //console.log('writeFile: Write completed.');
                        fileEntry.file(writeCallback,function(e) {
                            console.log('writeFile: Error', e);
                        });
                    };
                    fileWriter.write(blob);
                }, function(e) {
                    console.log('writeFile: Error', e);
                });
            }, function(e) {
                console.log('writeFile: Error', e);
            });
        }, function(e) {
            console.log('writeFile: Error', e);
        });
    }

    appendFile = function(fs,blob,appendCallback) {
        //console.log('appendFile: ' + blob.name + ' is ' + blob.size + ' bytes ');

        var fn = blob.name.split('/').join('-');
        //console.log('appendFile: fn ' + fn );

        fs.root.getDirectory('music', {create: true}, function(dirEntry) {
            dirEntry.getFile(fn, {create: true, exclusive: false}, function(fileEntry) {

                // Create a FileWriter object for our FileEntry, and append the blob.
                fileEntry.createWriter(function(fileWriter) {
                    fileWriter.onerror = function(e) {
                        console.log('writeFile: Error', e);
                    };
                    fileWriter.onwriteend = function(e) {
                        //console.log('writeFile: Write completed.');
                        fileEntry.file(appendCallback,function(e) {
                            console.log('writeFile: Error', e);
                        });
                    };
                    fileWriter.seek(fileWriter.length); // Start write position at EOF.

                    fileWriter.write(blob);
                }, function(e) {
                    console.log('writeFile: Error', e);
                });

            }, function(e) {
                console.log('writeFile: Error', e);
            });
        }, function(e) {
            console.log('writeFile: Error', e);
        });
    }



    // Received new cover from server!
    coverclient.on('stream', function(stream, meta) {
        console.log('coverclient.on stream: Recieving new cover ' + meta);
        // Buffer for parts
        var parts = [];
        // Got new data
        stream.on('data', function(data) {
            parts.push(data);
        });
        stream.on('end', function() {
            // Display new data in browser!

            console.log('coverclient.on stream: finished getting cover data for ' + meta);

            var coverblob = new Blob(parts);
            coverblob.name = meta;
            //console.log('coverclient.on stream: cover ' + coverblob.name + ' size ' + coverblob.size);
            saveFile(coverblob,setCover);

        });
    });

    // Received new music from server!
    musicclient.on('stream', function(stream, meta) {
        console.log('musicclient.on stream: Recieving new music ' + meta);
        var fs = null;
        var fn = meta.split('/').join('-');
        // Buffer for parts
        var parts = [];

        allAudioBufferSourceNode.forEach(function(sourceNode) {
            sourceNode.stop()
        });

        allAudioBufferSourceNode = [];
        startTime = 0;

        /* make sure the audioContext is not suspended */
        audioContext.resume().then(function() {
            $scope.currentoperation = 'Pause';
            $scope.$apply();
        });

        // Got new data
        stream.on('data', function(data) {
            parts.push(data);

            /* We are streaming, play now */

            audioContext.decodeAudioData(data, function(buffer) {
                queueChunkToPlay(buffer);

                startTime += buffer.duration;
            });
        });
        stream.on('end', function() {
            console.log('musicclient.on stream: finished getting data for ' + fn);

            var musicblob = new Blob(parts);
            musicblob.name = meta;
            //console.log('coverclient.on stream: cover ' + coverblob.name + ' size ' + coverblob.size);
            saveFile(musicblob, function() {
                //console.log('appendCallBack: appended a chunk')
            });


        });
    });

    $scope.onPlayPauseClicked = function(file) {
        if(audioContext.state == 'running') {
            audioContext.suspend().then(function() {
                $scope.currentoperation = 'Play';
                $scope.$apply();
            });
        } else {
            audioContext.resume().then(function() {
                $scope.currentoperation = 'Pause';
                $scope.$apply();
            });
        }
    };

    $scope.onitemClicked = function(file) {
        var encodedfilename = encodeURIComponent(file.filename);
        console.log('onitemClicked: ' + encodedfilename + ' ' + file.type);

        // lets retrieve the contents
        // should I decide here, or on the server what to do with the data hmmm?
        if(file.type == 'dir') {
            var fn = '';

            // this is a little dodgy, someone could perhaps get ../../../etc/passwd
            if(encodedfilename == '..') {
                var slashIndex = $scope.currentdir.lastIndexOf('/');
                fn = $scope.currentdir.substring(0,slashIndex);
            } else {
                fn = encodedfilename;
            }
            $scope.currentdir = decodeURIComponent(fn);

            console.log('onitemClicked: requesting file list for ' + fn);

            $http.get('/getfiles?path=' + fn)
            .success(function(data) {
                $scope.data = data;
                $scope.status = 'onitemClicked: Got file list for ' + fn;
                console.log($scope.status);
                var c = '';

                for(var i=0; i<data.length; i++) {
                    console.log(data[i].type);

                    if(data[i].type == 'cover') {
                        console.log(data[i].filename);

                        c = data[i].filename;
                    }
                }

                if(c == '') {
                    c = 'nocover.jpg';
                }

                console.log('onitemClicked: requesting cover from filelisting ' + c);

                findOrRequestFile(c,
                function(fn) {
                    $http.get('/getcover?path=' + fn)
                    .success(function(data) {
                        $scope.status = data;
                        console.log('onitemClicked: ' + data);
                    })
                    .error(function(data) {
                        $scope.status = 'onitemClicked: Error: ' + data;
                        console.log($scope.status);
                    });
                },
                function(fn) {
                    window.webkitRequestFileSystem(PERSISTENT, $scope.mincachebump,
                    function(fsobject) {
                        fsobject.root.getDirectory('music', {create: true}, function(dirEntry) {
                            // dont think I need this var dirReader = dirEntry.createReader();

                            dirEntry.getFile(fn, {create: true, exclusive: false}, function(fileEntry) {
                                fileEntry.file(function(fb) {
                                    console.log('FOUND LOCAL COVER ' + fn);
                                    setCover(fb);
                                });
                            });
                        });
                    });
                });

                console.log('current cover will be:' + $scope.currentcover);
            })
            .error(function(data) {
                $scope.status = 'onitemClicked: Error: ' + data;
                console.log($scope.status);
            });

        } else if(file.type == 'music') {
            console.log('onitemClicked: requesting music ' + file.filename);

            findOrRequestFile(file.filename,
            function(fn) {
                console.log('gotta get a file from the server');
                $http.get('/getfile?path=' + fn)
                .success(function(data) {
                    $scope.status = data;
                    console.log('onitemClicked: ' + data);
                    $scope.currenttrack = file.filename;
                })
                .error(function(data) {
                    $scope.status = 'onitemClicked: Error: ' + data;
                    console.log($scope.status);
                    $scope.currenttrack = '';
                });
            },
            function(fn) {
                console.log('gotta get a file out of the local cache');
                window.webkitRequestFileSystem(PERSISTENT, $scope.mincachebump,
                function(fsobject) {
                    fsobject.root.getDirectory('music', {create: true}, function(dirEntry) {

                        dirEntry.getFile(fn, {create: true, exclusive: false}, function(fileEntry) {
                            fileEntry.file(function(fb) {
                                console.log('LOAD FILE NOW ' + fn);

                                var reader = new FileReader();

                                reader.onloadend = function(e) {
                                    console.log('PLAY FILE NOW ' + fn);

                                    audioContext.decodeAudioData(this.result, function(buffer) {
                                        allAudioBufferSourceNode.forEach(function(sourceNode) {
                                            sourceNode.stop()
                                        });

                                        allAudioBufferSourceNode = [];
                                        startTime = 0;

                                        /* make sure the audioContext is not suspended */
                                        audioContext.resume().then(function() {
                                            $scope.currentoperation = 'Pause';
                                            $scope.$apply();
                                        });
                                        $scope.currenttrack = file.filename;
                                        queueChunkToPlay(buffer);
                                    });

                                };

                                reader.readAsArrayBuffer(fb);
                            });
                        });
                    });
                });
            }
                             );
        } else if(file.type == 'cover') {
            console.log('onitemClicked: requesting cover ' + file.filename);

            findOrRequestFile(file.filename,
            function(fn) {
                $http.get('/getcover?path=' + fn)
                .success(function(data) {
                    $scope.status = data;
                    console.log('onitemClicked: ' + data);
                })
                .error(function(data) {
                    $scope.status = 'onitemClicked: Error: ' + data;
                    console.log($scope.status);
                });
            },
            function(fn) {
                window.webkitRequestFileSystem(PERSISTENT, $scope.mincachebump,
                function(fsobject) {
                    fsobject.root.getDirectory('music', {create: true}, function(dirEntry) {
                        var dirReader = dirEntry.createReader();

                        dirEntry.getFile(fn, {create: true, exclusive: false}, function(fileEntry) {
                            fileEntry.file(function(fb) {
                                console.log('SET COVER NOW ' + fn);
                                setCover(fb);
                            });
                        });
                    });
                });
            });
        }

    };

}]);












